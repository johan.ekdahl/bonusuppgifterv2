package com.company;

import java.util.ArrayList;
import java.util.List;
import java.util.*;

public class QandAclass {

    List<String> questions = new ArrayList<>();
    String[] question = {"1. Alongside former President Frederik Willem de Klerk, which then President of South Africa won a Nobel prize for their work to peacefully end apartheid in the country?",

            "2. How many cards are there in a pack of cards (not incl. jokers)?",

            "3. What does www stand for?",

            "4. What Saints Day is celebrated (particularly in Ireland) on 17th March each year?",

            "5. Where might you find Tower Bridge and Buckingham Palace, amongst many other landmarks?",

            "6. What is the name of Mickey Mouse’s partner?",

            "7. What country is famous for inventing the Taco, Burrito and Quesadilla?",

            "8. Who is the South African born CEO of SpaceX and the brains behind Tesla?",

            "9. What animal is Pumbaa in the Lion King?",

            "10. What company is responsible for the iPhone, iPad and iWatch and many other products?"};
    List<String> answers = new ArrayList<>();
    String[] answer = {"Nelson Mandela",
            "52",
            "World wide web",
            "St Patricks Day",
            "London",
            "Minnie Mouse",
            "Mexico",
            "Elon Musk",
            "A warthog",
            "Apple"};

    public QandAclass() {

    }



}

